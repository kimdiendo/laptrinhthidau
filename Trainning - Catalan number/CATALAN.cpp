/*  Demo for catalan number
    Useful problems:
    - Balanced parenthese problems
    - Dyck paths problems
    - Non-intersecting arcs prolems
    - Lattice paths prolems
    - Non-crossing Muraski diagram
 */

#include <bits/stdc++.h>

using namespace std;

int main() {
    unsigned long long cat = 1;
    cout << cat << " ";

    for (int i = 2; i <= 33; i++)
        cat = cat*(4*i - 6)/i,
        cout << cat << " ";

    printf("\nTime elapsed: %.5lf ms\n", (double) 1000 * clock() / CLOCKS_PER_SEC);
    return 0;
}
