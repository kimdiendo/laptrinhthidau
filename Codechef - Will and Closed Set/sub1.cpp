#include <bits/stdc++.h>

using namespace std;

const int N = 15;
const int M = 1000000007;

int C[N + 1][N + 1], GCD[N + 1][N + 1];
int A[N];
int n, k, L, base, mask;

void solve() {
  mask = base = 0;
  n = k = L = 0;
  memset(A, 0, sizeof A);

  scanf("%d %d %d\n", &n, &k, &L);
  for (int i = 1; i <= n; i++) {
    scanf("%d ", &A[i]);
    base |= (1 << (A[i] - 1));
  }

  for (int i = 1; i < n; i++)
    for (int j = i + 1; j <= n; j++) {
      int c = GCD[A[i]][A[j]];
      if (c > L) {
        printf("0\n");
        return;
      }
      if (!((base >> (c - 1)) & 1))
        base |= (1 << (c - 1)),
        k--;
    }

    if (k == 0) {
      printf("%d\n", 1);
      return;
    } else if (k < 0) {
      printf("%d\n", 0);
    }

    int ans = 0;
    for (int cnt = 1; cnt < (1 << L); cnt++) {
      if (__builtin_popcount(cnt) > k) continue;

      int mask = base;
      for (int j = 1; j <= N; j++)
        if ((cnt >> (j - 1)) & 1) mask |= (1 << (j - 1));

      bool ok = true;
      for (int i = 1; i < N; i++)
        for (int j = i + 1; j <= N; j++)
          if (((mask >> (i - 1)) & 1) && ((mask >> (j - 1)) & 1))
            if (!(mask >> (GCD[i][j] - 1) & 1)) ok = false;

      ans = ok ? (ans % M + C[k - 1][__builtin_popcount(cnt) - 1] % M) % M
               : ans;
    }

    printf("%d\n", ans);
}

int main() {
  #ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
  #endif // ONLINE_JUDGE

  for (int i = 0; i <= N; i++) C[i][0] = 1;
  for (int i = 1; i <= N; i++)
    for (int j = 1; j <= N; j++)
      C[i][j] = (C[i - 1][j - 1] % M + C[i - 1][j] % M) % M;

  for (int i = 1; i <= N; i++)
    for (int j = 1; j <= N; j++)
      GCD[i][j] = __gcd(i, j);

  int T;  scanf("%d\n", &T);
  while (T--) solve();
  
  #ifndef ONLINE_JUDGE
    fprintf(stderr, "\nTime elapsed: %d ms", 1000 * clock() / CLOCKS_PER_SEC);
  #endif // ONLINE_JUDGE

  return 0;
}
