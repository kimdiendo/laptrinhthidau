#include <bits/stdc++.h>

using namespace std;

const int N = 2001;
const int M = 1000000007;

int ans, n, m, A[N], B[N], C[N];
bool fr[N];

int check() {
  for (int k = 2; k <= m; k++)
    if (B[k] + A[C[k]] < B[k-1] + A[C[k-1]]) return 0;
  return 1;
}

void backtrack(int i) {
  for (int j = C[i - 1] + 1; j <= n; j++) {
    if (fr[j] == false) {
      C[i] = j;
      if (i == m) ans += check();
      else {
        fr[j] = true;
        backtrack(i + 1);
        fr[j] = false;
      }
    }
  }
}

int main() {
  #ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
  #endif //ONLINE_JUDGE
  scanf("%d %d\n", &n, &m);
  for (int i = 1; i <= n; i++) scanf("%d ", &A[i]);
  for (int i = 1; i <= m; i++) scanf("%d ", &B[i]);

  backtrack(1);
  printf("%d\n", ans);
  return 0;
}
