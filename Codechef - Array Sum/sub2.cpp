#include <bits/stdc++.h>

using namespace std;

const int N = 2001;
const int inf = 0x3f3f3f3f;
const int M = 1000000007;

int ans, n, m, A[N], B[N], C[N];
long long dp[N][N];

int main() {
  #ifndef ONLINE_JUDGE
    freopen("in.txt", "r", stdin);
  #endif //ONLINE_JUDGE
  scanf("%d %d\n", &n, &m);
  for (int i = 1; i <= n; i++) scanf("%d ", &A[i]);
  for (int i = 1; i <= m; i++) scanf("%d ", &B[i]);

  dp[0][0] = 1; A[0] = B[0] = -inf;
  for (int i = 1; i <= n; i++)
    for (int j = 1; j <= m; j++) {
      dp[i][j] = 0;
      for (int k = 0; k < i; k++)
        if (A[i] + B[j] >= A[k] + B[j-1])
          dp[i][j] = (dp[i][j] % M + dp[k][j-1] % M) % M;
    }

  long long ans = 0;
  for (int i = 1; i <= n; i++) ans = (ans % M + dp[i][m] % M) % M;
  printf("%lld\n", ans);

  return 0;
}
