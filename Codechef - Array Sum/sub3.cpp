#include <cassert>
#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

const int MAXN = 2000 + 5;
const int MOD = 1000000000 + 7;

int dp[MAXN][MAXN], fw[MAXN + MAXN], wn, w[MAXN + MAXN];
int n, m, a[MAXN], b[MAXN];

void modify (int j, int x) {
    while (j <= wn) {
        fw[j] = (fw[j] + x) % MOD;
        j = j + j - (j & (j - 1));
    }
}

int findsum (int j) {
    int ret = 0;
    while (j > 0) {
        ret = (ret + fw[j]) % MOD;
        j = (j & (j - 1));
    }
    return ret;
}

int main(int argc, const char * argv[]) {
    cin >> n >> m;
    assert(1 <= n && n <= 2000);
    assert(1 <= m && m <= 1000);
    assert(m <= n);
    for(int i = 1; i <= n; i++) {
        cin >> a[i];
        assert(1 <= a[i] && a[i] <= 1000000000);
    }
    for(int i = 1; i <= m; i++) {
        cin >> b[i];
        assert(1 <= b[i] && b[i] <= 1000000000);
    }
    for(int i = 1; i <= n; i++)
        dp[1][i] = 1;
    for(int i = 2; i <= m; i++) {
        wn = 0;
        for(int j = 1; j <= n; j++) {
            w[++wn] = b[i - 1] + a[j];
            w[++wn] = b[i] + a[j];
        }
        sort(w + 1, w + wn + 1);
        wn = (int)(unique(w + 1, w + wn + 1) - (w + 1));
        for(int j = 1; j <= wn; j++)
            fw[j] = 0;
        for(int j = 1; j <= n; j++) {
            dp[i][j] = findsum((int)(lower_bound(w + 1, w + wn + 1, a[j] + b[i]) - w));
            modify((int)(lower_bound(w + 1, w + wn + 1, a[j] + b[i - 1]) - w), dp[i - 1][j]);
        }
    }
    int ret = 0;
    for(int i = 1; i <= n; i++)
        ret = (ret + dp[m][i]) % MOD;
    cout << ret << endl;
    return 0;
}
