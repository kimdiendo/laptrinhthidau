#include <bits/stdc++.h>

#define task "B"

using namespace std;

const int N = 101;
const int inf = 0x3f3f3f3f;

bool isPrime(int x) {
  if (x < 2) return false;
  for (int i = 2; i * i <= x; i++)
    if (x % i == 0) return false;
  return true;
}

int n;
unsigned int dp[N][1 << 17];
int pd[N][1 << 17], mask[60], ans[N];
pair<int, int> a[N];

int main() {
  #ifndef ONLINE_JUDGE
    assert(freopen(task".INP", "r", stdin));
    assert(freopen(task".OUT", "w", stdout));
  #endif // ONLINE_JUDGE

  ios_base::sync_with_stdio(0); cin.tie(NULL);

  vector <int> primes;
  for (int i = 2; i < 60; i++)
    if (isPrime(i)) primes.emplace_back(i);
  for (int i = 1; i < 60; i++)
    for (int j = 0; j < (int) primes.size(); j++)
      if (i % primes[j] == 0)
        mask[i] |= (1 << j);

  memset(dp, -1, sizeof (unsigned int) * N * (1 << 17));
  scanf("%d", &n);
  for (int i = 0; i < n; i++) {
    int t;  scanf("%d", &t);
    a[i] = {t, i};
  }

  sort(a, a + n);
  int bd = max(0, n - 17);
  dp[0][0] = 0;
  for (int i = 0; i < bd; i++)
    dp[i + 1][0] = a[i].first - 1 + dp[i][0],
    pd[i + 1][0] = 1;

  for (int i = bd; i < n; i++) {
    for (int j = 0; j < (1 << 17); j++) {
      if (dp[i][j] == -1) continue;
      for (int k = 1; k < 60; k++)
        if ((mask[k] & j) == 0) {
          int nmask = mask[k] | j;
          if (dp[i + 1][nmask] > dp[i][j] + abs(a[i].first - k)) {
            dp[i + 1][nmask] = dp[i][j] + abs(a[i].first - k);
            pd[i + 1][nmask] = k;
          }
        }
    }
  }

  int x = -1, nm = 1e9;
  for (int i = 0; i < (1 << 17); i++) {
    if (dp[n][i] < nm) {
      nm = dp[n][i];
      x = i;
    }
  }
  for (int i = n - 1; i >= 0; i--) {
    ans[a[i].second] = pd[i + 1][x];
    x ^= mask[pd[i + 1][x]];
  }

  for (int i = 0; i < n; i++) printf("%d ", ans[i]);

  #ifndef ONLINE_JUDGE
    fprintf(stderr, "\nTime elapsed: %d ms", 1000 * clock() / CLOCKS_PER_SEC);
  #endif // ONLINE_JUDGE
}
