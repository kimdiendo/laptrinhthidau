#include <bits/stdc++.h>

#define task "DP"
#define _LOCAL_

using namespace std;

const int inf = 0x3f3f3f3f;
const int M = 100000;
const int N = 20;

int n, m, col[M];
char s[M];

class SlowSolution {
public:
  int solve() {
    int ret = inf;
    for (int mask = 0; mask < (1 << n); mask++) {
      int temp = 0;
      for (int j = 0; j < m; j++) {
        int x = col[j] ^ mask;
        int cnt = __builtin_popcount(x);
        temp += min(cnt, n - cnt);
      }
      ret = min(ret, temp);
    }
    return ret;
  }
};

class AcceptedSolution {

};

int main() {
#ifdef _LOCAL_
  assert(freopen(task".INP", "r", stdin));
  assert(freopen(task".OUT", "w", stdout));
#endif // _LOCAL_

  scanf("%d%d", &n, &m);
  for (int i = 0; i < n; i++) {
    scanf(" %s", s);
    for (int j = 0; j < m; j++)
      col[j] |= (s[j] - '0') << i;
  }

  SlowSolution Task;
  printf("%d", Task.solve());

#ifdef _LOCAL_
  fprintf(stderr, "\nTime elapsed: %d ms", 1000 * clock() / CLOCKS_PER_SEC);
#endif // _LOCAL_
}
